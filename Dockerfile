FROM alpine:3.19
LABEL maintainer "Brian Cole <docker@brianecole.com>"

ARG VERSION=3.2

RUN mkdir /opt/cosmos && \
    cd /opt/cosmos && \
    wget https://cosmo.zip/pub/cosmos/zip/cosmos-${VERSION}.zip && \
    unzip cosmos-${VERSION}.zip && \
    rm cosmos-${VERSION}.zip && \
    ls && \
    /opt/cosmos/bin/bash --version && \
    : ;

#ENV PATH=/opt/cosmos/bin:$PATH
#CMD /opt/cosmos/bin/bash
