# Cosmos container image

This is totally unofficial and unaffiliated with the cosmo/cosmos/APE devs.


## What

This repo contains source code to build an OCI image containing cosmos (as
described on https://justine.lol/cosmo3/ ) layered over Alpine Linux.


## Why

Among other reasons, there are some cases where containers are easier to run
even than bare binaries (CI, mostly).


## TODO

* Test that it actually works.
* Can/should I make the binaries thinner and or assimilate them? In this context
  they really only need to run on Linux (or ABI equivalents - looking at you,
  illumos).
* Set CMD to BASH


## License

This repo is MIT (see LICENSE file), but note that that only covers the contents
of this repo (builds scripts, basically), not the contents of the generated
image!

